<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ArtikelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('artikel')->insert([
            ['judul' => '6 Simbol pada Label Baju ini Ada Artinya, Mulai dari Aturan Pencucian Hingga Cara Menyetrika', 
            'isi' => 'Tanda atau simbol pada label baju sering dianggap sepele, apalagi ukurannya yang kecil buat sebagian orang merasa bukan hal yang penting-penting banget. Tapi sebenarnya setiap simbol pada pakaian ada artinya lo, mulai dari perawatan saat mencuci sampai pengeringannya.

            Tujuan dari label baju ini sebetulnya sebagai aturan supaya pakaian lebih awet, kamu pun jadi lebih tahu pemeliharaannya yang benar berdasarkan bahan pakaian tersebut. Ini juga jadi salah satu kunci sederhana buatmu yang sering mengalami kelunturan pada baju, jangan langsung salahkan bahannya yang buruk, bisa saja karena kamu belum perhatikan simbol label yang tertera di belakangnya. Nah, berikut ini ragam simbol pada pakaian yang perlu kamu tahu sebelum mengambil tindakan.',
            'thumbnail' => 'https://cdn-image.hipwee.com/wp-content/uploads/2021/01/hipwee-Fotoram.io-36.jpg'],
            ['judul' => '9 Bahan Kaos Olahraga dari yang Murah Sampai Mahal. Jangan Asal, Cocokkan dengan Aktivitasnya', 
            'isi' => 'Banyak sekali jenis olahraga yang bisa kita lakukan baik di dalam rumah maupun di sekitar lingkungan rumah. Dari mulai work out sampai bersepeda. Sebenarnya nggak ada peraturan khusus tentang pakaian atau kaos olahraga yang harus dikenakan saat beraktivitas. Hanya saja, olahraga akan lebih maksimal dan nyaman jika kita mengenakan kaos olahraga keren — yang tentunya disesuaikan dengan aktivitasnya.
            Selain menambah estetika berpenampilan, rupanya bahan kaos olahraga itu berbeda-beda dan banyak jenisnya lo. Berikut ini adalah 9 bahan kaos beserta olahraga yang cocok untuk mengenakan kaos dengan bahan-bahan tersebut. Nggak bisa asal pilih ternyata~
            
            1. Hyget adalah bahan kain yang paling murah. Biasanya digunakan untuk bahan kaos partai yang teksturnya mengilap
            ',
            'thumbnail' => 'https://cdn-image.hipwee.com/wp-content/uploads/2021/01/hipwee-collage-14.png'],
        	
        ]);
    }
}
