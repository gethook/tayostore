<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
        	['title' => 'employee', 'nama_role' => 'Employee/Admin'],
        	['title' => 'customer', 'nama_role' => 'Customer/Pelanggan'],
        ]);
    }
}
