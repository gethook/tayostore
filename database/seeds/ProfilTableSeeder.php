<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfilTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = DB::table('users')->where('name', 'jadoel')->first();
        DB::table('profil')->insert([
        	'nama_lengkap' => 'Wong Jadoel',
        	'phone' => '081234567890',
        	'user_id' => $user->id,
        ]);
    }
}
