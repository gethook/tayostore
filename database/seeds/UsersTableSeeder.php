<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $emp = DB::table('roles')->where('title', 'employee')->first();
        DB::table('users')->insert([
        	'name' => 'jadoel',
        	'email' => 'jadoel@mail.id',
        	'password' => Hash::make('wongjadoel'),
        	'role_id' => $emp->id,
        ]);
    }
}
