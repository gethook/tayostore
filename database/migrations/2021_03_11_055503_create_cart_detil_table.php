<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartDetilTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_detil', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('kuantitas')->default(1);
            $table->decimal('harga', 10, 0);
            $table->timestamps();
        });

        Schema::table('cart_detil', function (Blueprint $table) {
            $table->unsignedBigInteger('cart_id');
            $table->unsignedBigInteger('produk_id');
            $table->foreign('cart_id')->references('id')->on('cart');
            $table->foreign('produk_id')->references('id')->on('produk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_detil');
    }
}
