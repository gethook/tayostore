<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\Kategori;
use App\GaleriProduk;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Auth;

class ProdukController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$products = Produk::with(['galeri' => function ($query) {
    		$query->orderBy('foto_utama', 'desc');
    	}])->get();
    	return view('backend.pages.produk.produk.index', compact('products'));
    }

    public function create()
    {
    	if (!$this->is_employee()) {
    		return redirect('/');
    	}
    	$categories = Kategori::all();
    	return view('backend.pages.produk.produk.create', compact('categories'));
    }

    public function save(Request $request)
    {
    	if (!$this->is_employee()) {
    		return redirect('/');
    	}
    	$request->validate([
    		'nama_produk' => 'required|unique:produk|string|min:3',
    		'desc' => 'required',
    		'harga' => 'required|numeric|min:0',
    		'stok' => 'required|numeric|min:0',
    	]);
    	$galeri = explode(',', $request->galeri);
    	// dd($galeri);
    	$produk = Produk::create([
    		'nama_produk' => $request->nama_produk,
    		'desc' => $request->desc,
    		'harga' => $request->harga,
    		'stok' => $request->stok
    	]);
    	$produk->kategori()->sync($request->kategori);
    	foreach ($galeri as $key => $foto) {
    		$produk->galeri()->save(
    			new GaleriProduk([
    				'foto' => $foto
    			])
    		);
    	}
    	// $produk->galeri()->
    	Alert::toast('Produk berhasil disimpan.', 'success');
    	return redirect(route('produk'));//->with('sukses', 'Produk berhasil ditambahkan.');
    }

    public function edit($id)
    {
    	if (!$this->is_employee()) {
    		return redirect('/');
    	}
    	$produk = Produk::find($id);
    	$categories = Kategori::all();
    	return view('backend.pages.produk.produk.edit', compact(['produk','categories']));    	
    }

    public function update($id, Request $request)
    {
    	if (!$this->is_employee()) {
    		return redirect('/');
    	}
    	$request->validate([
    		'nama_produk' => 'required|string|min:3',
    		'desc' => 'required',
    		'harga' => 'required|numeric|min:0',
    		'stok' => 'required|numeric|min:0',
    	]);
    	$produk = Produk::find($id);
    	$produk->nama_produk = $request->nama_produk;
    	$produk->desc = $request->desc;
    	$produk->harga = $request->harga;
    	$produk->stok = $request->stok;
    	$produk->update();

    	$galeri = explode(',', $request->galeri);
    	$produk->galeri()->delete();
    	foreach ($galeri as $key => $foto) {
    		$produk->galeri()->save(
    			new GaleriProduk([
    				'foto' => $foto
    			])
    		);
    	}

    	$produk->kategori()->sync($request->kategori);

    	Alert::toast('Produk berhasil diubah.', 'success');
    	return redirect(route('produk'));//->with('sukses', 'Produk berhasil ditambahkan.');
    }

    public function destroy($id)
    {
    	if (!$this->is_employee()) {
    		return redirect('/');
    	}
    	$produk = Produk::find($id);
    	$produk->galeri()->delete();
    	$produk->kategori()->detach();
    	$produk->delete();
    	Alert::toast('Produk berhasil dihapus.', 'error');
    	return redirect(route('produk'));//->with('sukses', 'Produk berhasil ditambahkan.');
    }

    public function show($id)
    {
    	$produk = Produk::find($id);
    	return view('backend.pages.produk.produk.show', compact('produk'));
    }

    protected function is_employee()
    {
    	$user = Auth::user();
    	return ($user->role->title == 'employee');
    }

}
