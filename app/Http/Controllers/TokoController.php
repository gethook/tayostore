<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Artikel;
class TokoController extends Controller
{
    public function index(){
        return view('frontend.layouts.index');
    }
    public function blog(){
        $artikel = DB::table('artikel')->get();
        // dd($artikel);
        return view('frontend.pages.articles.bloglist', compact('artikel'));
    }
    public function blogshow(){ //belum ditambahi id untuk menampilkan detail artikel
        return view('frontend.pages.articles.blogshow');
    }

}
