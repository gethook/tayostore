<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Auth;

class KategoriController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //
    public function index()
    {
    	$categories = Kategori::all();
    	return view('backend.pages.produk.kategori.index', compact('categories'));
    }

    public function create()
    {
        if (!$this->is_employee()) {
            return redirect('/');
        }
    	return view('backend.pages.produk.kategori.create');
    }

    public function show($id)
    {
    	$kategori = Kategori::find($id);
    	return view('backend.pages.produk.kategori.show', compact('kategori'));
    }

    public function save(Request $request)
    {
        if (!$this->is_employee()) {
            return redirect('/');
        }
    	$request->validate([
    		'nama_kategori' => 'required|unique:kategori|string|min:3',
    		'desc' => 'required'
    	]);
    	$save = Kategori::create([
    		'nama_kategori' => $request->nama_kategori,
    		'desc' => $request->desc,
    		'foto' => $request->foto,
    	]);
        Alert::toast('Kategori produk berhasil disimpan.', 'success');
    	return redirect(route('produk_kategori'))->with('sukses', 'Kategori produk berhasil ditambahkan.');
    }

    public function edit($id)
    {
        if (!$this->is_employee()) {
            return redirect('/');
        }
    	$kategori = Kategori::find($id);
    	return view('backend.pages.produk.kategori.edit', compact('kategori'));
    }

    public function update($id, Request $request)
    {
        if (!$this->is_employee()) {
            return redirect('/');
        }
    	$request->validate([
    		'nama_kategori' => 'required|string|min:3',
    		'desc' => 'required'
    	]);
    	$kategori = Kategori::find($id);
    	$kategori->nama_kategori = $request->nama_kategori;
    	$kategori->desc = $request->desc;
    	$kategori->foto = $request->foto;
    	$kategori->update();
        Alert::toast('Kategori produk berhasil diubah.', 'success');
    	return redirect(route('produk_kategori'))->with('sukses', 'Kategori produk berhasil diubah.');
    }

    public function destroy($id)
    {
        if (!$this->is_employee()) {
            return redirect('/');
        }
    	$kategori = Kategori::find($id);
    	$kategori->delete();
        Alert::toast('Kategori ' . $kategori->nama_kategori . ' berhasil dihapus', 'error');
    	return redirect(route('produk_kategori'))->with('sukses', 'Kategori produk berhasil dihapus.');
    }

    protected function is_employee()
    {
        $user = Auth::user();
        return ($user->role->title == 'employee');
    }
}
