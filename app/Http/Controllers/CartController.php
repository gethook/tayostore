<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use App\Produk;
use Illuminate\Support\Facades\Auth;
use DB;

class CartController extends Controller
{

    public function add(Request $request)
    {
    	// ($request->session()->forget('cart_id'));
    	// ($request->session()->save());
    	// dd($request->session()->get('cart_count'));
    	$user_id = $request->user() ? $request->user()->id : null;
		$produk_id = $request->produk_id;
		$produk = Produk::find($produk_id);
		// dd($request->session()->get('cart_id'));
		if (!$request->session()->has('cart_id')) {
			$cart = new Cart();
			$cart->status = 'shopping';
			$cart->user_id = $user_id;
			$cart->save();
			$request->session()->put('cart_id', $cart->id);
			$request->session()->save();
		} else {
			$cart = Cart::find($request->session()->get('cart_id'));
		}
		$pic = DB::table('cart_detil')->where([
			['cart_id', '=', $cart->id],
			['produk_id', '=', $produk_id]
		])->first();
		if ($pic) {
			$update = DB::table('cart_detil')->where('id', $pic->id)->update([
				'kuantitas' => ($pic->kuantitas + 1)
			]);
		} else {
			$cart->products()->attach($produk_id, [
				'kuantitas' => 1,
				'harga' => $produk->harga,
			]);
		}
		$request->session()->put('cart_count', count($cart->products));
		$request->session()->save();
    	// dd($produk);
    	return redirect()->back();
    }
}
