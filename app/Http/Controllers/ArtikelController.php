<?php

namespace App\Http\Controllers;

use App\Artikel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class ArtikelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    $artikel= DB::table("artikel") -> get();
        // dd($artikel);
        return view('backend.pages.articles.post', compact('artikel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judulartikel'=> 'required',
            'isiartikel'=> 'required',            
        ]);
        // dd($request->all());
        $blog = new Artikel;
        $blog->judul = $request->judulartikel;
        $blog->isi = $request->isiartikel;
        $blog->thumbnail = $request->imageartikel;
        $blog->slug = Str::slug($request->judulartikel);
        $blog->user_id = Auth::id();
        $blog->save();
        
        return redirect(route('artikel.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Artikel  $artikel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $artikel = DB::table('artikel')->where('id', $id)->first();
        return view('backend.pages.articles.show', compact('artikel')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Artikel  $artikel
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $artikel= Artikel::find($id);
        return view('backend.pages.articles.edit', compact('artikel'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Artikel  $artikel
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {

        $update = artikel::find($id);
        $update->judul = $request->judulartikel;
        $update->isi = $request->isiartikel;
        $update->update();
        return redirect()->route('artikel.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Artikel  $artikel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $artikel = artikel::find($id);
        $artikel->delete();
        return redirect('/artikel');
    }
}
