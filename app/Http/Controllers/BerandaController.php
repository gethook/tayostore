<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\Kategori;

class BerandaController extends Controller
{
    public function index()
    {
    	$products = Produk::all();
    	$categories = Kategori::all();
        return view('frontend.pages.beranda', compact(['products', 'categories']));
    }
}
