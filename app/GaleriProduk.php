<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GaleriProduk extends Model
{
    protected $table = 'galeri_produk';
    protected $fillable = ['foto', 'foto_utama'];

    public function produk()
    {
    	return $this->belongsTo('App\Produk');
    }
}
