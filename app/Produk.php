<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    //
    protected $table = 'produk';
    protected $fillable = ['nama_produk', 'desc', 'harga', 'stok'];

    public function kategori()
    {
    	return $this->belongsToMany('App\Kategori', 'kategori_produk', 'produk_id', 'kategori_id');
    }

    public function galeri()
    {
    	return $this->hasMany('App\GaleriProduk');
    }
}
