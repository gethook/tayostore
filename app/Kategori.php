<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    //
    protected $table = 'kategori';
    protected $fillable = ['nama_kategori', 'desc', 'foto'];

    public function products()
    {
    	return $this->belongsToMany('App\Produk', 'kategori_produk', 'kategori_id', 'produk_id');
    }
}
