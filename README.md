<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Informasi Kelompok

Kelompok 03

Anggota:

- Akhmad Syarifudin
- Tangkas Wahyu
- Yoga Pratama Sakti

Tema: Web E-commerce dengan sinkronasi backend(admin) dan front end

## Aditional Items

ERD

<img src="https://lh5.googleusercontent.com/HGoClDlX_KWulk8VMoocZgve8DMseiDy4Fa0rMCce1yevjcffuGjd7z-srf4s0ZXjSktU_ZUxP0E2elb8ONB=w1440-h764-rw">

Link: ('https://drive.google.com/file/d/1UhW2QLvwpPzENFVAnoKsErQoPUofvIqx/view')

Video: (https://drive.google.com/file/d/104-CSoIIpiSfrLBZvDi9zRVMQPFG_dzP/view?usp=sharing)
