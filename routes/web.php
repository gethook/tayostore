<?php
use Illuminate\Database\Eloquent\Model;
use App\Artikel;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();
//======Route untuk Front-End
Route::get('/home', 'HomeController@index')->name('home');

//======Beranda Front-End
// Route::get('/', 'BerandaController@index')->name('beranda');
Route::get('/', 'BerandaController@index')->name('beranda');

//======Kategori Front-End
Route::get('/kategori', function () {
    return view('frontend.pages.kategori');
});
//=====Akhir Route untuk Front-End

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

//=====Route untuk admin (Backend)

Route::get('/admin', 'AdminController@index')->name('admin.index');
// Route::get('/admin/post')

//======dashboard
// Route::get('/', 'DashboardController@index')->name('dashboard');

//======categori
// Route::get('/categori', 'CategoriController@index')->name('categori');

// //======cart
// Route::get('/cart', 'CartController@index')->name('cart');

// //product_list
// Route::get('/product_list', 'Product_listController@index')->name('product_list');

//=====Akhir Route untuk Front-End

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

//=====Route untuk admin (Backend)

Route::get('/admin', 'AdminController@index')->name('admin.index');

// Untuk Post di Blog
Route::resource('artikel', 'ArtikelController');

// Produk dan Kategori
Route::get('/admin/produk/kategori', 'KategoriController@index')->name('produk_kategori');
Route::get('/admin/produk/kategori/tambah', 'KategoriController@create')->name('tambah_produk_kategori');
Route::post('/admin/produk/kategori', 'KategoriController@save');
Route::get('/admin/produk/kategori/{id}/edit', 'KategoriController@edit')->name('ubah_produk_kategori');
Route::put('/admin/produk/kategori/{id}', 'KategoriController@update')->name('update_produk_kategori');
Route::get('/admin/produk/kategori/{id}', 'KategoriController@show')->name('view_produk_kategori');
Route::delete('/admin/produk/kategori/{id}', 'KategoriController@destroy')->name('hapus_produk_kategori');

Route::get('/admin/produk', 'ProdukController@index')->name('produk');
Route::get('/admin/produk/tambah', 'ProdukController@create')->name('tambah_produk');
Route::post('/admin/produk', 'ProdukController@save');
Route::get('/admin/produk/{id}', 'ProdukController@show')->name('view-produk');
Route::get('/admin/produk/{id}/edit', 'ProdukController@edit')->name('ubah-produk');
Route::put('/admin/produk/{id}', 'ProdukController@update')->name('update-produk');
Route::delete('/admin/produk/{id}', 'ProdukController@destroy')->name('hapus-produk');
// Route::get('/testdb', function(){            //Saya gunakan untuk mengetes kondisi tabel
//     $test = DB::table('artikel') -> get();
//     dd($test);
// });

// Route::get('/show', function () {           //Saya gunakan untuk tes halaman
//     return view('backend.pages.articles.show');
// });

//====Akhir Route untuk admin


//====Route untuk Toko
// Route::get('/', 'TokoController@index');
Route::get('/blog', 'TokoController@blog');
Route::get('/blog/show', 'TokoController@blogshow')->name('blog.show');

Route::get('/show', function () {
    return view('backend.pages.articles.show');
});

Route::post('/atc', 'CartController@add')->name('atc');

//====Akhir Route untuk admin
//====Akhir Route untuk Toko

//====Akhir Route untuk admin

//====Akhir Route untuk Toko
