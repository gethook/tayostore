@extends('backend.layouts.adminmaster')

@section('content')
<div class="box box-primary ml-3 mr-4">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Artikel {{$artikel->id}}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="/artikel/{{$artikel->id}}" method="POST">
            @csrf
            @method('PUT')
              <div class="box-body">
                <div class="form-group">
                  <label for="judul">Judul Artikel</label>
                  <input type="text" class="form-control" id="judulartikel" placeholder="Masukan judul" name="judulartikel" value="{{$artikel->judul}}">
                </div>
                <div class="form-group">
                  <label>Isi</label>
                  <textarea class="form-control" rows="3" placeholder="Enter ..." name="isiartikel" >{{$artikel->isi}}</textarea>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <input type="file" id="exampleInputFile" name="imageartikel" value="{{$artikel->thumbnail}}">

                  <p class="help-block">Example block-level help text here.</p>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Edit</button>
              </div>
            </form>
          </div>

 @endsection