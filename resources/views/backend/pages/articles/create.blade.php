@extends('backend.layouts.adminmaster')

@section('content')
<div class="box box-primary ml-3 mr-4">
            <div class="box-header with-border">
              <h3 class="box-title">Form Artikel</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ route('artikel.store') }}" method="POST">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="judul">Judul Artikel</label>
                  <input type="text" class="form-control" id="judulartikel" placeholder="Masukan judul" name="judulartikel">
                </div>
                <div class="form-group">
                  <label>Isi</label>
                  <textarea class="form-control" rows="3" placeholder="Enter ..." name="isiartikel"></textarea>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <input type="file" id="exampleInputFile" name="imageartikel">

                  <p class="help-block">Example block-level help text here.</p>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox"> Check me out
                  </label>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>

 @endsection