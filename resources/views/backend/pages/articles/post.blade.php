@extends('backend.layouts.adminmaster')

@section('content')
<div class="container-fluid">
 <div class="d-sm-flex align-items-center justify-content-between mb-4">
 <h1 class="h3 mb-0 text-gray-800">Post Management</h1>
 <a href="{{ route('artikel.create')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-bullhorn fa-sm text-white-50"></i> Create Post</a>
 </div>
 <div class="card shadow mb-4">
     <div class="card-header py-3">
         <h6 class="m-0 font-weight-bold text-primary">Data artikel</h6>
     </div>
     <div class="card-body">
         <div class="table-responsive">
             <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
              <div class="row">
               <div class="col-sm-12 col-md-6">
                <div class="dataTables_length" id="dataTable_length">
                 <label>Show <select name="dataTable_length" aria-controls="dataTable" class="custom-select custom-select-sm form-control form-control-sm">
                  <option value="10">10</option>
                  <option value="25">25</option>
                  <option value="50">50</option>
                  <option value="100">100</option></select> 
                  entries</label>
                 </div>
                </div>
               <div class="col-sm-12 col-md-6">
                <div id="dataTable_filter" class="dataTables_filter">
                 <label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="dataTable"></label>
                </div></div>
               </div>
               <div class="row">
                <div class="col-sm-12">
                 <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                 <thead>
                     <tr role="row">
                      <th class="sorting_asc" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 149px;">Judul</th>
                      <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 247px;">Isi</th>
                      <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 100px;">Thumbnails</th>
                      <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 90px;">Menu</th>
                     </tr>
                 </thead>
                 <tfoot>
                     <tr>
                      <th rowspan="1" colspan="1">Judul</th>
                      <th rowspan="1" colspan="1">Isi</th>
                      <th rowspan="1" colspan="1">Thumbnails</th>
                      <th rowspan="1" colspan="1">Menu</th>
                     </tr>
                 </tfoot>
                 <tbody>
                 @forelse ($artikel as $key=>$artikel)
                    <tr role="row" class="odd">
                        <!-- <td>{{$key + 1}}</th> -->
                        <td>{{$artikel->judul}}</td>
                        <td>{{$artikel->isi}}</td>
                        <td>{{$artikel->thumbnail}}</td>
                        <td>
                            <a href="/artikel/{{$artikel->id}}" class="btn btn-info">Show</a>
                            <a href="/artikel/{{$artikel->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/artikel/{{$artikel->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1 d-inline" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse    
                    </tbody>
             </table>
            </div>
           </div>
           <div class="row">
            <div class="col-sm-12 col-md-5">
             <div class="dataTables_info" id="dataTable_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries
             </div>
            </div>
            <div class="col-sm-12 col-md-7">
             <div class="dataTables_paginate paging_simple_numbers" id="dataTable_paginate">
              <ul class="pagination">
               <li class="paginate_button page-item previous disabled" id="dataTable_previous"><a href="#" aria-controls="dataTable" data-dt-idx="0" tabindex="0" class="page-link">Previous</a>
              </li>
              <li class="paginate_button page-item active"><a href="#" aria-controls="dataTable" data-dt-idx="1" tabindex="0" class="page-link">1</a>
             </li>
             <li class="paginate_button page-item "><a href="#" aria-controls="dataTable" data-dt-idx="2" tabindex="0" class="page-link">2</a>
            </li>
             <li class="paginate_button page-item "><a href="#" aria-controls="dataTable" data-dt-idx="3" tabindex="0" class="page-link">3</a>
            </li>
            <li class="paginate_button page-item "><a href="#" aria-controls="dataTable" data-dt-idx="4" tabindex="0" class="page-link">4</a>
           </li>
           <li class="paginate_button page-item "><a href="#" aria-controls="dataTable" data-dt-idx="5" tabindex="0" class="page-link">5</a>
          </li>
          <li class="paginate_button page-item "><a href="#" aria-controls="dataTable" data-dt-idx="6" tabindex="0" class="page-link">6</a>
         </li>
         <li class="paginate_button page-item next" id="dataTable_next"><a href="#" aria-controls="dataTable" data-dt-idx="7" tabindex="0" class="page-link">Next</a>
        </li>
       </ul>
      </div>
     </div>
    </div>
   </div>
         </div>
     </div>
 </div>
</div>



@endsection