@extends('backend.layouts.adminmaster')

@section('content')
<div class="card shadow mb-4">
 <div class="card-header py-3">
     <h6 class="m-0 font-weight-bold text-primary">{{$artikel->judul}}</h6>
 </div>
 <div class="card-body">
     {{$artikel->isi}}
 </div>
</div>


@endsection