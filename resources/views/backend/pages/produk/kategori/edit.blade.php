@extends('backend.layouts.adminmaster')

@section('html-title')
	Ubah Kategori Produk
@endsection

@section('content')
	{{-- expr --}}
	<h1 class="h3 mb-2">Kategori Produk </h1>
	<div class="card shadow mb-4">
	    <div class="card-header py-3">
	        <h6 class="m-0 font-weight-bold text-primary">Ubah</h6>
	    </div>
	    <div class="card-body">
	    	<form action="{{ route('update_produk_kategori', $kategori->id) }}" method="post">
	    		@csrf
	    		@method('PUT')
	    		<div class="form-group">
	    			<label for="nama">Kategori</label>
	    			<input type="text" name="nama_kategori" id="nama" class="form-control @error('nama_kategori') is-invalid @enderror" value="{{ old('nama_kategori', $kategori->nama_kategori) }}">

	    			@error('nama_kategori')
	    			    <span class="invalid-feedback" role="alert">
	    			        <strong>{{ $message }}</strong>
	    			    </span>
	    			@enderror

	    		</div>
	    		<div class="form-group">
	    			<label for="desc">Deskripsi</label>
	    			<textarea name="desc" class="form-control my-editor @error('desc') is-invalid @enderror">{!! old('desc', $kategori->desc) !!}</textarea>

	    			@error('desc')
	    			    <span class="invalid-feedback" role="alert">
	    			        <strong>{{ $message }}</strong>
	    			    </span>
	    			@enderror

	    		</div>
	    		<div class="form-group">
	    			<label for="foto">Foto</label>
	    			<div class="input-group">
	    			   <span class="input-group-btn">
	    			     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
	    			       <i class="fa fa-picture-o"></i> Choose
	    			     </a>
	    			   </span>
	    			   <input id="thumbnail" class="form-control" type="text" name="foto" value="{{ old('foto', $kategori->foto)}}">
	    			 </div>
	    			 <div id="holder" style="margin-top:15px;max-height:100px;">
	    			 	@if ($kategori->foto)
	    			 		<img src="{{ $kategori->foto }}" alt="" style="height: 5rem;">
	    			 	@endif
	    			 </div>
	    		</div>
	    		<div class="form-group row">
	    			<div class="col-md-12">
	    				<button class="btn btn-primary" type="submit">
	    					<i class="fas fa-save fa-fw"></i>
	    					Simpan
	    				</button>
	    				<a href="{{ route('produk_kategori') }}" class="btn btn-info">
	    					<i class="fas fa-undo fa-fw"></i>
	    					Batal
	    				</a>
	    			</div>
	    		</div>
	    	</form>
	    </div>
	</div>
@endsection
@push('html-heads')
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
@endpush
@push('scripts')
	<script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
	<script>
		$('#lfm').filemanager('image');
	  var editor_config = {
	    path_absolute : "/",
	    selector: "textarea.my-editor",
	    plugins: [
	      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
	      "searchreplace wordcount visualblocks visualchars code fullscreen",
	      "insertdatetime media nonbreaking save table contextmenu directionality",
	      "emoticons template paste textcolor colorpicker textpattern"
	    ],
	    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
	    relative_urls: false,
	    file_browser_callback : function(field_name, url, type, win) {
	      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
	      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

	      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
	      if (type == 'image') {
	        cmsURL = cmsURL + "&type=Images";
	      } else {
	        cmsURL = cmsURL + "&type=Files";
	      }

	      tinyMCE.activeEditor.windowManager.open({
	        file : cmsURL,
	        title : 'Filemanager',
	        width : x * 0.8,
	        height : y * 0.8,
	        resizable : "yes",
	        close_previous : "no"
	      });
	    }
	  };

	  tinymce.init(editor_config);
	</script>
@endpush