@extends('backend.layouts.adminmaster')

@section('html-title')
	Kategori Produk
@endsection

@section('content')
	<h1 class="h3 mb-2">Kategori</h1>
	<div class="card shadow mb-4">
	    <div class="card-header py-3">
	        <h6 class="m-0 font-weight-bold text-primary">Kategori Produk</h6>
	    </div>
	    <div class="card-body">
	    	@if (session('sukses'))
	    		<div class="alert alert-success alert-dismissable">
	    			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	    			{{ session('sukses') }}
	    		</div>
	    	@endif
	    	<a href="{{ route('tambah_produk_kategori') }}" class="btn btn-primary mb-3">
	    		<i class="fas fa-plus fa-fw"></i>
	    		Tambah Kategori
	    	</a>
	        <div class="table-responsive">
	            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
	                <thead>
	                    <tr>
	                        <th>No</th>
	                        <th>Kategori</th>
	                        <th>Deskripsi</th>
	                        <th>Foto</th>
	                        <th>Actions</th>
	                    </tr>
	                </thead>
	                <tbody>
	                	@forelse ($categories as $key => $kategori)
	                		<tr>
	                			<td>{{ $key+1 }}</td>
	                			<td>{{ $kategori->nama_kategori }}</td>
	                			<td>{{ strip_tags($kategori->desc) }}</td>
	                			<td><img src="{{ $kategori->foto }}" alt="" style="max-height: 100px;"></td>
	                			<td>
	                				<a href="{{ route('view_produk_kategori', $kategori->id) }}" class="btn btn-sm mb-1 btn-info">
	                					View
	                				</a>
	                				<a href="{{ route('ubah_produk_kategori', $kategori->id) }}" class="btn btn-sm mb-1 btn-primary">
	                					Ubah
	                				</a>
	                				<form action="{{ route('hapus_produk_kategori', $kategori->id) }}" method="post" class="form-hapus" onsubmit="return konfirmasiHapus(this)">
	                					@csrf
	                					@method('DELETE')
	                					<button type="submit" class="btn btn-sm mb-1 btn-danger" id="hapus-{{$kategori->id}}">Hapus</button>
	                				</form>

	                			</td>
	                		</tr>
	                	@empty
	                		{{-- empty expr --}}
	                	@endforelse
	                </tbody>
	            </table>
	        </div>
	    </div>
	</div>

@endsection
@push('scripts')
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script>
		function konfirmasiHapus(form) {
			swal({
				title: "Apakah Anda Yakin Akan Menghapus Kategori Ini?",
				text: "Kategori yang sudah dihapus tidak dapat dikembalikan",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((terkonfirmasi) => {
				if (terkonfirmasi) {
					form.submit();
				}
			});
			return false;
		}
	</script>
@endpush