@extends('backend.layouts.adminmaster')

@section('html-title')
	Kategori Produk
@endsection

@section('content')
	<h1 class="h3 mb-2">Kategori Produk </h1>
	<div class="card shadow mb-4">
	    <div class="card-header py-3">
	        <h6 class="m-0 font-weight-bold text-primary">{{ $kategori->nama_kategori }}</h6>
	    </div>
	    <div class="card-body">
	    	<div class="row">
	    		<div class="col-md-4">
	    			<img src="{{ $kategori->foto }}" alt="{{ $kategori->nama_kategori }}">
	    		</div>
	    		<div class="col-md-8">
	    			{!! $kategori->desc !!}
	    		</div>
	    	</div>
	    	
	    </div>
	    <div class="card-footer">
	    	<a href="{{ route('produk_kategori') }}" class="btn btn-info">
	    		<i class="fas fa-reply fa-fw"></i>
	    		Kembali
	    	</a>
	    </div>
	</div>
@endsection