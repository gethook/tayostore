@extends('backend.layouts.adminmaster')

@section('html-title')
	Produk
@endsection

@section('content')
	<h1 class="h3 mb-2">View Produk </h1>
	<div class="card shadow mb-4">
	    <div class="card-header py-3">
	        <h6 class="m-0 font-weight-bold text-primary">{{ $produk->nama_produk }}</h6>
	    </div>
	    <div class="card-body">
	    	<h5 class="">Deskripsi</h5>
	    	{!! $produk->desc !!}
	    	<h5 class="mt-2">Kategori:</h5>
	    	<ul>
	    		@forelse ($produk->kategori as $kategori)
	    			<li>{{ $kategori->nama_kategori }}</li>
	    		@empty
	    			{{-- empty expr --}}
	    		@endforelse
	    	</ul>
	    	<h5 class="mt-2">Galeri Produk</h5>
	    	<div class="row">
	    		@forelse ($produk->galeri()->orderBy('foto_utama', 'desc')->get() as $galeri)
	    			<div class="col-md-3">
	    				<div class="card m-1" style="max-height: 17rem; max-width: 14rem;">
	    					<img src="{{ $galeri->foto }}" alt="{{ $produk->nama_produk }}" style="max-height: 100%;">
	    				</div>
	    			</div>
	    		@empty
	    			{{-- empty expr --}}
	    		@endforelse
	    	</div>
	    </div>
	    <div class="card-footer">
	    	<a href="{{ route('produk') }}" class="btn btn-info">
	    		<i class="fas fa-reply fa-fw"></i>
	    		Kembali
	    	</a>
	    </div>
	</div>
@endsection