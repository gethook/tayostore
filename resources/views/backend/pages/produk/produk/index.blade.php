@extends('backend.layouts.adminmaster')

@section('html-title')
	Produk
@endsection

@section('content')
	<h1 class="h3 mb-2">Produk</h1>
	<div class="card shadow mb-4">
	    <div class="card-header py-3">
	        <h6 class="m-0 font-weight-bold text-primary">List Produk</h6>
	    </div>
	    <div class="card-body">
	    	@if (session('sukses'))
	    		<div class="alert alert-success alert-dismissable">
	    			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	    			{{ session('sukses') }}
	    		</div>
	    	@endif
	    	<a href="{{ route('tambah_produk') }}" class="btn btn-primary mb-3">
	    		<i class="fas fa-plus fa-fw"></i>
	    		Tambah Produk
	    	</a>
	        <div class="table-responsive">
	            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
	                <thead>
	                    <tr>
	                        <th>No</th>
	                        <th></th>
	                        <th>Nama Produk</th>
	                        <th>Kategori</th>
	                        <th>Deskripsi</th>
	                        <th>Harga</th>
	                        <th>Stok</th>
	                        <th>Actions</th>
	                    </tr>
	                </thead>
	                <tbody>
	                	@forelse ($products as $key => $produk)
	                		<tr>
	                			<td>{{ $key+1 }}</td>
	                			<td> 
	                				@if (count($produk->galeri) > 0)
	                					<img src="{{ $produk->galeri[0]->foto }}" alt="{{ $produk->nama_produk }}" style="height: 5rem;">
	                				@endif
	                				{{-- {{ $produk->galeri()->orderBy('foto_utama', 'DESC')->first() }} --}} <img src="" alt=""> </td>
	                			<td>{{ $produk->nama_produk }}</td>
	                			<td>
	                				@forelse ($produk->kategori as $kategori)
	                					<a href="{{ route('view_produk_kategori',$kategori->id) }}" class="btn btn-outline-dark btn-sm">{{ $kategori->nama_kategori }}</a>
	                					{{-- <span class="badge badge-info">{{ $kategori->nama_kategori }}</span> --}}
	                				@empty
	                					{{-- empty expr --}}
	                				@endforelse
	                			</td>
	                			<td>{{ strip_tags($produk->desc) }}</td>
	                			<td class="text-right">Rp{{ number_format($produk->harga,0,',','.') }}</td>
	                			<td class="text-right">{{ number_format($produk->stok,0,',','.') }}</td>
	                			<td>
	                				<a href="{{ route('view-produk', $produk->id) }}" class="btn btn-sm mb-1 btn-info">
	                					View
	                				</a>
	                				<a href="{{ route('ubah-produk', $produk->id) }}" class="btn btn-sm mb-1 btn-primary">
	                					Ubah
	                				</a>
	                				<form action="{{ route('hapus-produk', $produk->id) }}" method="post" class="form-hapus" onsubmit="return konfirmasiHapus(this)">
	                					@csrf
	                					@method('DELETE')
	                					<button type="submit" class="btn btn-sm mb-1 btn-danger" id="hapus-{{$produk->id}}">Hapus</button>
	                				</form>
{{-- 	                				<a href="{{ route('view_produk', $produk->id) }}" class="btn btn-sm mb-1 btn-info">
	                					View
	                				</a>
 --}}
	                			</td>
	                		</tr>
	                	@empty
	                		{{-- empty expr --}}
	                	@endforelse
	                </tbody>
	            </table>
	        </div>
	    </div>
	</div>

@endsection
@push('scripts')
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script>
		function konfirmasiHapus(form) {
			swal({
				title: "Apakah Anda Yakin Akan Menghapus Produk Ini?",
				text: "Produk yang sudah dihapus tidak dapat dikembalikan",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((terkonfirmasi) => {
				if (terkonfirmasi) {
					form.submit();
				}
			});
			return false;
		}
	</script>
@endpush