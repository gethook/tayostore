@extends('backend.layouts.adminmaster')

@section('html-title')
	Ubah Produk
@endsection

@section('content')
	{{-- expr --}}
	<h1 class="h3 mb-2">Produk </h1>
	<div class="card shadow mb-4">
	    <div class="card-header py-3">
	        <h6 class="m-0 font-weight-bold text-primary">Ubah Produk</h6>
	    </div>
	    <div class="card-body">
	    	<form action="{{ route('update-produk', $produk->id) }}" method="post">
	    		@csrf
	    		@method('PUT')
	    		<div class="form-group">
	    			<label for="nama">Nama Produk</label>
	    			<input type="text" name="nama_produk" id="nama" class="form-control @error('nama_produk') is-invalid @enderror" value="{{ old('nama_produk', $produk->nama_produk) }}">

	    			@error('nama_produk')
	    			    <span class="invalid-feedback" role="alert">
	    			        <strong>{{ $message }}</strong>
	    			    </span>
	    			@enderror

	    		</div>
	    		<div class="form-group">
	    			<label for="desc">Deskripsi</label>
	    			<textarea name="desc" class="form-control my-editor @error('desc') is-invalid @enderror">{!! old('desc', $produk->desc) !!}</textarea>

	    			@error('desc')
	    			    <span class="invalid-feedback" role="alert">
	    			        <strong>{{ $message }}</strong>
	    			    </span>
	    			@enderror

	    		</div>
	    		<div class="form-group">
	    			<label for="harga">Harga</label>
	    			<input type="text" id="harga" name="harga" class="form-control @error('harga') is-invalid @enderror" value="{{ old('harga', $produk->harga) }}">

	    			@error('harga')
	    			    <span class="invalid-feedback" role="alert">
	    			        <strong>{{ $message }}</strong>
	    			    </span>
	    			@enderror
	    		</div>
	    		<div class="form-group">
	    			<label for="stok">Stok</label>
	    			<input type="text" id="stok" name="stok" class="form-control @error('stok') is-invalid @enderror" value="{{ old('stok', $produk->stok) }}">

	    			@error('stok')
	    			    <span class="invalid-feedback" role="alert">
	    			        <strong>{{ $message }}</strong>
	    			    </span>
	    			@enderror
	    		</div>
	    		<div class="form-group">
	    			<label for="kategori">Kategori</label>
	    			<select class="form-control js-example-basic-multiple" name="kategori[]" multiple="multiple">
	    				@forelse ($categories as $kategori)
	    					<option value="{{ $kategori->id }}" 
	    						@forelse ($produk->kategori as $pk)
	    							@if ($kategori->id == $pk->id)
	    								selected="" 
	    							@endif
	    						@empty
	    							{{-- empty expr --}}
	    						@endforelse
	    						>{{ $kategori->nama_kategori }}</option>
	    				@empty
	    					{{-- empty expr --}}
	    				@endforelse
	    			</select>	    		
	    		</div>
	    		<div class="form-group">
	    			<label for="galeri">Galeri</label>
	    			@php
	    				$photos = [];
	    			@endphp
	    				@forelse ($produk->galeri as $galeri)
	    					@php $photos[] = $galeri->foto; @endphp
	    				@empty
	    					
	    				@endforelse
	    			@php
	    				$str_foto = implode(',', $photos);
	    			@endphp
	    			<div class="input-group">
	    			   <span class="input-group-btn">
	    			     <a id="lfm" data-input="galeri" data-preview="holder" class="btn btn-primary">
	    			       <i class="fa fa-picture-o"></i> Choose
	    			     </a>
	    			   </span>
	    			   <input id="galeri" class="form-control" type="text" name="galeri" value="{{ old('galeri', $str_foto)}}">
	    			 </div>
	    			 <div id="holder" style="margin-top:15px;max-height:100px;">
	    			 	@forelse ($photos as $foto)
	    			 		<img src="{{ $foto }}" alt="" style="height: 5rem;">
	    			 	@empty
	    			 		{{-- empty expr --}}
	    			 	@endforelse
	    			 </div>
	    		</div>
	    		<div class="form-group row">
	    			<div class="col-md-12">
	    				<button class="btn btn-primary" type="submit">
	    					<i class="fas fa-save fa-fw"></i>
	    					Simpan
	    				</button>
	    				<a href="{{ route('produk') }}" class="btn btn-info">
	    					<i class="fas fa-undo fa-fw"></i>
	    					Batal
	    				</a>
	    			</div>
	    		</div>
	    	</form>
	    </div>
	</div>
@endsection
@push('html-heads')
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush
@push('scripts')
	<script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#lfm').filemanager('image');
		    $('.js-example-basic-multiple').select2();
		    var editor_config = {
		      path_absolute : "/",
		      selector: "textarea.my-editor",
		      plugins: [
		        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
		        "searchreplace wordcount visualblocks visualchars code fullscreen",
		        "insertdatetime media nonbreaking save table contextmenu directionality",
		        "emoticons template paste textcolor colorpicker textpattern"
		      ],
		      toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
		      relative_urls: false,
		      file_browser_callback : function(field_name, url, type, win) {
		        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
		        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

		        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
		        if (type == 'image') {
		          cmsURL = cmsURL + "&type=Images";
		        } else {
		          cmsURL = cmsURL + "&type=Files";
		        }

		        tinyMCE.activeEditor.windowManager.open({
		          file : cmsURL,
		          title : 'Filemanager',
		          width : x * 0.8,
		          height : y * 0.8,
		          resizable : "yes",
		          close_previous : "no"
		        });
		      }
		    };

		    tinymce.init(editor_config);
		    
		});
	</script>
@endpush