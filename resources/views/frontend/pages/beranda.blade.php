@extends('frontend.layouts.master')
@section('content')
    <main>
        <section class="latest-product-area padding-bottom">
            <div class="container">
                <div class="row product-btn d-flex justify-content-end align-items-end">
                    <!-- Section Tittle -->
                    <div class="col-xl-4 col-lg-5 col-md-5">
                        <div class="section-tittle mb-30">
                            <h2>Produk</h2>
                        </div>
                    </div>
                    <div class="col-xl-8 col-lg-7 col-md-7">
                        <div class="properties__button f-right">
                            <!--Nav Button  -->
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">All</a>
                                    @forelse ($categories as $kategori)
                                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-{{ $kategori->id }}" role="tab" aria-controls="nav-profile" aria-selected="false">{{ $kategori->nama_kategori }}</a>
                                    @empty
                                        {{-- empty expr --}}
                                    @endforelse
                                    
                                </div>
                            </nav>
                            <!--End Nav Button  -->
                        </div>
                    </div>
                </div>
                <!-- Nav Card -->
                <div class="tab-content" id="nav-tabContent">
                    <!-- card one -->
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="row">
                            @forelse ($products as $produk)
                                <div class="col-xl-4 col-lg-4 col-md-6">
                                    <div class="single-product mb-60">
                                        <div class="product-img">
                                            @if (count($produk->galeri) > 0)
                                                <img src="{{ $produk->galeri[0]->foto }}" alt="">
                                            @else
                                                <img src="notfound" alt="Not Found">
                                            @endif
                                        </div>
                                        <div class="product-caption">
                                            {{-- <div class="product-ratting">
                                                <i class="far fa-star"></i>
                                                <i class="far fa-star"></i>
                                                <i class="far fa-star"></i>
                                                <i class="far fa-star low-star"></i>
                                                <i class="far fa-star low-star"></i>
                                            </div> --}}
                                            <h4><a href="#">{{ $produk->nama_produk }}</a></h4>
                                            <div class="price">
                                                <span style="font-size: 18px; font-weight: 500;">Rp{{ number_format($produk->harga,0,',','.') }}</span>
                                            </div>
                                            <div class="mt-2">
                                                <form action="{{ route('atc') }}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="produk_id" value="{{ $produk->id }}">
                                                    <button type="submit" class="btn_3">Beli</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @empty
                                <h3>Belum ada produk</h3>
                            @endforelse
                        </div>
                    </div>
                    <!-- Card two -->
                    @forelse ($categories as $cat)
                        <div class="tab-pane fade" id="nav-{{ $cat->id }}" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <div class="row">
                                @forelse ($cat->products as $p)
                                    <div class="col-xl-4 col-lg-4 col-md-6">
                                        <div class="single-product mb-60">
                                            <div class="product-img">
                                                @if (count($p->galeri))
                                                    <img src="{{ $p->galeri[0]->foto }}" alt="">
                                                @else
                                                    <img src="nf" alt="Not Found">
                                                @endif
                                            </div>
                                            <div class="product-caption">
                                                <div class="product-ratting">
                                                    <i class="far fa-star"></i>
                                                    <i class="far fa-star"></i>
                                                    <i class="far fa-star"></i>
                                                    <i class="far fa-star low-star"></i>
                                                    <i class="far fa-star low-star"></i>
                                                </div>
                                                <h4><a href="#">{{ $p->nama_produk }}</a></h4>
                                                <div class="price">
                                                    <ul>
                                                        <li>Rp{{ number_format($p->harga,0,',','.') }}</li>
                                                        {{-- <li class="discount">$60.00</li> --}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @empty
                                    {{-- empty expr --}}
                                @endforelse
                            </div>
                        </div>
                    @empty
                        {{-- empty expr --}}
                    @endforelse
                </div>
                <!-- End Nav Card -->
            </div>
        </section>
        <!-- Latest Products End -->
        <div class="shop-method-area">
            <div class="container">
                <div class="row d-flex justify-content-between">
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="single-method mb-40">
                            <i class="ti-search"></i>
                            <h6>Mudah</h6>
                            <p>Mencari Barang</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="single-method mb-40">
                            <i class="ti-time"></i>
                            <h6>Efisien</h6>
                            <p>Hemat Waktu</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="single-method mb-40">
                            <i class="ti-lock"></i>
                            <h6>Sistem Pembayaran</h6>
                            <p>Aman</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="single-method mb-40">
                            <i class="ti-package"></i>
                            <h6>Metode Pengiriman</h6>
                            <p>JNE, JNT, LION EXPRESS</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Shop Method End-->
        <!-- Gallery Start-->
        <div class="gallery-wrapper lf-padding">
            <div class="gallery-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="gallery-items">
                            <img src="assets/img/gallery/gallery1.jpg" alt="">
                        </div>
                        <div class="gallery-items">
                            <img src="assets/img/gallery/gallery2.jpg" alt="">
                        </div>
                        <div class="gallery-items">
                            <img src="assets/img/gallery/gallery3.jpg" alt="">
                        </div>
                        <div class="gallery-items">
                            <img src="assets/img/gallery/gallery4.jpg" alt="">
                        </div>
                        <div class="gallery-items">
                            <img src="assets/img/gallery/gallery5.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
