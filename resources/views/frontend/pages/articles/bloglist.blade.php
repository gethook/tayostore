@extends('frontend.layouts.frontmaster')
@section('content')

                       @foreach($artikel as $key=>$artikel)
                        <article class="blog_item container">
                            <div class="blog_item_img">
                                <img class="card-img rounded-0" src="{{$artikel->thumbnail}}" alt="">
                                <a href="#" class="blog_item_date">
                                    <h3>15</h3>
                                    <p>Jan</p>
                                </a>
                            </div>
                            <div class="blog_details">
                                <a class="d-inline-block" href="{{route('blog.show')}}">
                                    <h2>{{$artikel->judul}}</h2>
                                </a>
                                <p>{{$artikel->isi}}</p>
                                <ul class="blog-info-link">
                                    <li><a href="#"><i class="fa fa-user"></i> Travel, Lifestyle</a></li>
                                    <li><a href="#"><i class="fa fa-comments"></i> 03 Comments</a></li>
                                </ul>
                            </div>
                        </article>
                        @endforeach
                       
@endsection